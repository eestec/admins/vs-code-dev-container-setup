# VS Code Dev Container Setup
This Project, for now is an experiment of Alexander Geiger.
He would like minimize the steps necessary to get going
developing on EESTEC's software projects, starting with EESTECnet.
The approach followed for now is defining a [vs code dev container](https://code.visualstudio.com/docs/devcontainers/containers) setup, that includes the database, backend and frontend of EESTECnet. 
[here are some docs on multi-container setups](https://code.visualstudio.com/remote/advancedcontainers/connect-multiple-containers).